
#include <assert.h>

#include "Game.h"

using namespace std;
using namespace sf;

void Game::Init()
{
	srand(0);
	if (!font.loadFromFile("data/fonts/comic.ttf"))
		assert(false);
	
}

void Game::HandleInput(Uint32 key_, RenderWindow& window)
{
	key = key_;
	if (key == GC::ESCAPE_KEY)
		window.close();

	//Allow for creation of user name
	if (mode==Mode::ENTER_NAME && (isdigit(key) || isalpha(key)))
		name += static_cast<char>(key);
}

void Game::Update(RenderWindow& window)
{
	switch (mode)
	{
	case Mode::ENTER_NAME:
		if (key == GC::ENTER_KEY && !name.empty())
			mode = Mode::WELCOME;
		break;
	case Mode::WELCOME:
		UpdateWelcome();
		break;
	case Mode::GUESS:
		UpdateGuess();
		break;
	case Mode::GAME_OVER:
		if (key == GC::ENTER_KEY)
			window.close();
		break;
	default:
		assert(false);
	}
}

void Game::UpdateWelcome()
{
	if (key == GC::ENTER_KEY)
	{
		//Pick a word to guess
		wordIdx = rand() % 5;
		guess = ""; 
		for (int i = 0; i < (int)GC::WORDS[wordIdx].length(); ++i)
			guess += "_ "; //Set each letter to _
		mode = Mode::GUESS;
	}
}

void Game::CheckNewGuess()
{
	//see if the character is in the target word
	bool done = true;
	bool found = false;

	//loop checks each individual character against input to see if it is in the array
	for (int i = 0; i < (int)GC::WORDS[wordIdx].length(); ++i)
	{
		char tc = GC::WORDS[wordIdx][i]; //picks out a specific character in word
		if (tc == key)
		{
			found = true;
			guess[i * 2] = key; //replace _ with input letter, i*2 is used as each _ is 2 characters apart
		}

		if (tc != guess[i * 2]) //Checks each character to see if it has been guessed, ends game if they are all guessed
			done = false; 
	} 

	if (done)
	{
		//all words guessed
		won = true;
		mode = Mode::GAME_OVER;
	}

	if (!found)
	{
		//bad guess
		--lives;
		if (lives == 0)
		{
			won = false;
			mode = Mode::GAME_OVER;
		}
	}
	usedLetters.push_back(key); //Adds guessed letter onto vector of used letters
}

void Game::UpdateGuess()
{
	if (key != 0)
	{
		//should we ignore this one?
		bool usedBefore = false;
		for (int i = 0; i < (int)usedLetters.size(); ++i)
			if (usedLetters[i] == key) //If the letter inputted is the same as a character on vector of used letters
				usedBefore = true;
		if (!usedBefore) //If the input is a new letter
			CheckNewGuess();
	}
}

void Game::RenderEnterName(RenderWindow& window)
{
	string msg = "Enter your name (press return): "; //User can input name
	msg += name; //Adds name onto string 

	//Draw string
	Text txt2(msg, font, 20);
	txt2.setPosition(window.getSize().x * 0.2f, window.getSize().y * 0.1f);
	window.draw(txt2);
}

void Game::RenderWelcome(RenderWindow& window)
{
	//Draw first line
	string msg = "Welcome ";
	msg += name; //Adds name input onto string
	Text txt2(msg, font, 20);
	txt2.setPosition(window.getSize().x * 0.2f, window.getSize().y * 0.1f);
	window.draw(txt2);

	//Draw second line
	FloatRect fr = txt2.getLocalBounds();
	txt2.setPosition(txt2.getPosition().x, txt2.getPosition().y + fr.height * 2.f);
	txt2.setString("Try and guess each word presented, \nyou can make seven mistakes per word. <press return>");
	window.draw(txt2);
}

void Game::RenderGuess(RenderWindow& window)
{
	//Display lives
	string msg = "Lives left: ";
	msg += to_string(lives);
	Text txt2(msg, font, 20);
	txt2.setPosition(window.getSize().x * 0.2f, window.getSize().y * 0.15f);
	FloatRect fr = txt2.getLocalBounds();
	window.draw(txt2);

	//Display guess
	txt2.setPosition(txt2.getPosition().x, txt2.getPosition().y + fr.height * 2.f);
	msg = "Your guess: ";
	msg += guess;
	txt2.setString(msg);
	window.draw(txt2);
}

void Game::RenderGameOver(RenderWindow& window)
{
	//Draw the word the user had to guess
	string msg;
	msg = "Target word: " + GC::WORDS[wordIdx];
	Text txt(msg, font, 20);
	txt.setPosition(window.getSize().x * 0.2f, window.getSize().y * 0.15f);
	FloatRect fr = txt.getLocalBounds();
	window.draw(txt);

	//Draw results
	//If the user guessed the word
	if (won)
		msg = "Congratulations winner! :)";
	//If the user did not guess the word
	else
		msg = "Sorry, you lost :(";
	txt.setString(msg);
	txt.setPosition(txt.getPosition().x, txt.getPosition().y + fr.height * 2.f);
	window.draw(txt);

	//Draw exit message
	txt.setPosition(txt.getPosition().x, txt.getPosition().y + fr.height * 2.f);
	txt.setString("Press return to quit");
	window.draw(txt);
}
void Game::ShowTitle(RenderWindow& window)
{
	Text txt("Hangman", font, 30);
	txt.setFillColor(Color::White);
	txt.setOutlineColor(Color::Red);
	txt.setOutlineThickness(5);
	txt.setPosition(window.getSize().x * 0.2f, window.getSize().y * 0.05f);
	window.draw(txt);
}

void Game::Render(RenderWindow& window)
{
	ShowTitle(window);
	switch (mode)
	{
	case Mode::ENTER_NAME:
		RenderEnterName(window);
		break;
	case Mode::WELCOME:
		RenderWelcome(window);
		break;
	case Mode::GUESS:
		RenderGuess(window);
		break;
	case Mode::GAME_OVER:
		RenderGameOver(window);
		break;
	default:
		assert(false);
	}
	key = 0;
}
