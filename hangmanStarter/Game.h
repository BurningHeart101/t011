#pragma once

#include <string>
#include "SFML/Graphics.hpp"
#include "GameConsts.h"
#include <vector>

/*
A first stab at Hangman
Pick a random word from the library, let the player guess the letters
They've got 7 lives (failed guesses) before death
*/
struct Game
{
	enum class Mode {
		ENTER_NAME,		//get the player's name
		WELCOME,		//game instructions
		GUESS,			//player guesses at the word
		GAME_OVER		//game is over
	};

	Mode mode = Mode::ENTER_NAME; //current mode we are in
	sf::Font font;			//only need one font
	std::string name;		//player name
	char key = GC::NO_KEY;		//current key press 
	int lives = GC::MAX_LIVES; //die at 0 lives
	bool won = false; //checks if player has won
	std::string guess; //player's current guess
	int wordIdx = 0; //which word in the library are we using
	std::vector<char> usedLetters; //tracks every guess, will not count repeats

	//set things up once at the start
	void Init();
	//update the logic
	void Update(sf::RenderWindow& window);
	//render what we can see
	void Render(sf::RenderWindow& window);
	//check what the player is typing
	void HandleInput(sf::Uint32 key_, sf::RenderWindow& window);

	//player guesses target word
	void UpdateGuess();
	//player's guess character is new and needs checking, is it correct?
	void CheckNewGuess();
	//wait for enter, then chooose word they are trying to guess
	void UpdateWelcome();

	//ask the use what their name is
	void RenderEnterName(sf::RenderWindow& window);
	//explaining the game
	void RenderWelcome(sf::RenderWindow& window);
	//display guesses
	void RenderGuess(sf::RenderWindow& window);
	//game over
	void RenderGameOver(sf::RenderWindow& window);
	
	//every screen has the game name on the top	
	void ShowTitle(sf::RenderWindow& window);
};

